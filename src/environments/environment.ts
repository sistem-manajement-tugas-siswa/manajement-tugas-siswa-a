// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAmlRF--gPWBRqDFoZ8j5igmvmatrB0Yvw",
    authDomain: "manajement-tugas-siswa.firebaseapp.com",
    projectId: "manajement-tugas-siswa",
    storageBucket: "manajement-tugas-siswa.appspot.com",
    messagingSenderId: "658998479708",
    appId: "1:658998479708:web:0d570c82132a78445bcf99",
    measurementId: "G-TJX2V8PKMD"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
