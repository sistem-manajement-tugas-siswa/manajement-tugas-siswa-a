import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BuatTugasPage } from './buat-tugas.page';

describe('BuatTugasPage', () => {
  let component: BuatTugasPage;
  let fixture: ComponentFixture<BuatTugasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuatTugasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BuatTugasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
