import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuatTugasPage } from './buat-tugas.page';

const routes: Routes = [
  {
    path: '',
    component: BuatTugasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuatTugasPageRoutingModule {}
