import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuatTugasPageRoutingModule } from './buat-tugas-routing.module';

import { BuatTugasPage } from './buat-tugas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuatTugasPageRoutingModule
  ],
  declarations: [BuatTugasPage]
})
export class BuatTugasPageModule {}
