import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/service/toast.service';

@Component({
  selector: 'app-buat-tugas',
  templateUrl: './buat-tugas.page.html',
  styleUrls: ['./buat-tugas.page.scss'],
})
export class BuatTugasPage implements OnInit {

  dataTask : any = {};
  userData : any = {};
  constructor(
    private db : AngularFirestore,
    private auth : AngularFireAuth,
    public toast: ToastService,
    public router : Router
    
  ) { }

  ngOnInit() {
    this.auth.onAuthStateChanged(user=> {
      this.userData = user;
    }) 
  }

  loading: boolean;
  simpanTugas() 
  {
    this.loading = true;
    this.dataTask.author = this.userData.email;
    var doc = new Date().getTime().toString();
    this.db.collection('tasks').doc(doc).set(this.dataTask).then(res=>{
     this.toast.present('Tugas Baru Berhasil dibuat','top')
     this.router.navigate['./guru/tabs1']
    }).catch(err=>{
      this.loading = false;
      this.toast.present('Tugas gagal dibuat','top')
    })
  }

}
