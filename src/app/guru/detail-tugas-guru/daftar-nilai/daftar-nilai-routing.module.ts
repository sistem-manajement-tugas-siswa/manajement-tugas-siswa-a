import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DaftarNilaiPage } from './daftar-nilai.page';

const routes: Routes = [
  {
    path: '',
    component: DaftarNilaiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DaftarNilaiPageRoutingModule {}
