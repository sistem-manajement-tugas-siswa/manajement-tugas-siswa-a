import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DaftarNilaiPage } from './daftar-nilai.page';

describe('DaftarNilaiPage', () => {
  let component: DaftarNilaiPage;
  let fixture: ComponentFixture<DaftarNilaiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaftarNilaiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DaftarNilaiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
