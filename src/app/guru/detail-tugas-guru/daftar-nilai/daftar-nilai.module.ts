import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DaftarNilaiPageRoutingModule } from './daftar-nilai-routing.module';

import { DaftarNilaiPage } from './daftar-nilai.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DaftarNilaiPageRoutingModule
  ],
  declarations: [DaftarNilaiPage]
})
export class DaftarNilaiPageModule {}
