import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailTugasGuruPage } from './detail-tugas-guru.page';

const routes: Routes = [
  {
    path: '',
    component: DetailTugasGuruPage
  },
  {
    path: 'detail-pengirim',
    loadChildren: () => import('./detail-pengirim/detail-pengirim.module').then( m => m.DetailPengirimPageModule)
  },
  {
    path: 'daftar-nilai-siswa',
    loadChildren: () => import('./daftar-nilai-siswa/daftar-nilai-siswa.module').then( m => m.DaftarNilaiSiswaPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailTugasGuruPageRoutingModule {}
