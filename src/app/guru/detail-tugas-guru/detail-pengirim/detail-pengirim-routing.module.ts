import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailPengirimPage } from './detail-pengirim.page';

const routes: Routes = [
  {
    path: '',
    component: DetailPengirimPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailPengirimPageRoutingModule {}
