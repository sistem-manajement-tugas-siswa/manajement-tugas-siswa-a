import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailPengirimPage } from './detail-pengirim.page';

describe('DetailPengirimPage', () => {
  let component: DetailPengirimPage;
  let fixture: ComponentFixture<DetailPengirimPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailPengirimPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailPengirimPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
