import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailTugasGuruPageRoutingModule } from './detail-tugas-guru-routing.module';

import { DetailTugasGuruPage } from './detail-tugas-guru.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailTugasGuruPageRoutingModule
  ],
  declarations: [DetailTugasGuruPage]
})
export class DetailTugasGuruPageModule {}
