import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailTugasGuruPage } from './detail-tugas-guru.page';

describe('DetailTugasGuruPage', () => {
  let component: DetailTugasGuruPage;
  let fixture: ComponentFixture<DetailTugasGuruPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailTugasGuruPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailTugasGuruPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
