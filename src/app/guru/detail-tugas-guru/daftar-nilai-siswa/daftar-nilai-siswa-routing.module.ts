import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DaftarNilaiSiswaPage } from './daftar-nilai-siswa.page';

const routes: Routes = [
  {
    path: '',
    component: DaftarNilaiSiswaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DaftarNilaiSiswaPageRoutingModule {}
