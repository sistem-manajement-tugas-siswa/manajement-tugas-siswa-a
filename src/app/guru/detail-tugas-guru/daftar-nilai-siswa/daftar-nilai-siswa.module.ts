import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DaftarNilaiSiswaPageRoutingModule } from './daftar-nilai-siswa-routing.module';

import { DaftarNilaiSiswaPage } from './daftar-nilai-siswa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DaftarNilaiSiswaPageRoutingModule
  ],
  declarations: [DaftarNilaiSiswaPage]
})
export class DaftarNilaiSiswaPageModule {}
