import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DaftarNilaiSiswaPage } from './daftar-nilai-siswa.page';

describe('DaftarNilaiSiswaPage', () => {
  let component: DaftarNilaiSiswaPage;
  let fixture: ComponentFixture<DaftarNilaiSiswaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaftarNilaiSiswaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DaftarNilaiSiswaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
