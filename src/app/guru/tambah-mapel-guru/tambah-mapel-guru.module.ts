import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TambahMapelGuruPageRoutingModule } from './tambah-mapel-guru-routing.module';

import { TambahMapelGuruPage } from './tambah-mapel-guru.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TambahMapelGuruPageRoutingModule
  ],
  declarations: [TambahMapelGuruPage]
})
export class TambahMapelGuruPageModule {}
