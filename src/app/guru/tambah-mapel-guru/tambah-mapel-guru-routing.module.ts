import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TambahMapelGuruPage } from './tambah-mapel-guru.page';

const routes: Routes = [
  {
    path: '',
    component: TambahMapelGuruPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TambahMapelGuruPageRoutingModule {}
