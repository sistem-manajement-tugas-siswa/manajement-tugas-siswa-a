import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-tambah-mapel-guru',
  templateUrl: './tambah-mapel-guru.page.html',
  styleUrls: ['./tambah-mapel-guru.page.scss'],
})
export class TambahMapelGuruPage implements OnInit {

  dataMapel : any = {};
  userData : any = {};
  constructor(
    private db : AngularFirestore,
    private auth : AngularFireAuth
  ) { }

  ngOnInit() {
    this.auth.onAuthStateChanged(user=> {
      this.userData = user;
    }) 
  }

  loading: boolean;
  simpanMapel() 
  {
    this.loading = true;
    this.dataMapel.author = this.userData.email;
    var doc = new Date().getTime().toString();
    this.db.collection('mapels').doc(doc).set(this.dataMapel).then(res=>{
      alert('Mata Pelajaran Baru Berhasil dibuat')
    }).catch(err=>{
      this.loading = false;
      alert('Mata Pelajaran gagal dibuat')
    })
  }

}
