import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TambahMapelGuruPage } from './tambah-mapel-guru.page';

describe('TambahMapelGuruPage', () => {
  let component: TambahMapelGuruPage;
  let fixture: ComponentFixture<TambahMapelGuruPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TambahMapelGuruPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TambahMapelGuruPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
