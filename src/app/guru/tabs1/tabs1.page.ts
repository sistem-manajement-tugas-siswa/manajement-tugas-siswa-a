import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { ActionSheetController, LoadingController, ToastController } from '@ionic/angular';
import { AngularFireStorage } from '@angular/fire/storage'


interface Mapel {
  id: string;
  author:string;
  namaMapel : string;
  kelas : string;
  namaGuru : string;
  date_created: Date
}

@Component({
  selector: 'app-tabs1',
  templateUrl: './tabs1.page.html',
  styleUrls: ['./tabs1.page.scss'],
})
export class Tabs1Page implements OnInit {

  constructor(
    public actionSheetController: ActionSheetController,
    public router: Router,
    public auth: AngularFireAuth,
    public db: AngularFirestore,
    public storage: AngularFireStorage

  ) 
  {
    this.getAuthData();
   }
  ngOnInit() {
    
  }

  userdata: any = {};
  getAuthData()
  {
    this.auth.onAuthStateChanged(res=>{
      this.userdata = res;
      this.getUserData(res.email);      
      this.getData();
    })
  };

  getUserData(email)
  {
    this.db.collection('users').doc(email).get().subscribe(res=>{
      this.passRole(res.data());
    })
  }
  passRole(data)
  {
    this.userdata.role = data.role;
  }

  mapels: any = [];
  getData()
  {
    this.db.collection('mapels').valueChanges({idField: 'id'}).subscribe(res=>{
      this.mapels = res;
    });
  }

  

}
