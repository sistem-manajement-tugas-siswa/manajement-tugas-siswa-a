import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GuruPage } from './guru.page';

const routes: Routes = [
  {
    path: '',
    component: GuruPage,
    children : [
      {
        path: 'tabs1',
        loadChildren: () => import('./tabs1/tabs1.module').then( m => m.Tabs1PageModule)
      },
      {
        path: 'tabs2',
        loadChildren: () => import('./tabs2/tabs2.module').then( m => m.Tabs2PageModule)
      },
      {
        path: 'tabs3',
        loadChildren: () => import('./tabs3/tabs3.module').then( m => m.Tabs3PageModule)
      },
      {
        path: '',
        redirectTo: '/guru/tab1',
        pathMatch: 'full'
      }
    ]
  
  },
  {
    path: '',
    redirectTo: '/tabs/tab1',
    pathMatch: 'full'
  },
  {
    path: 'detail-tugas-guru',
    loadChildren: () => import('./detail-tugas-guru/detail-tugas-guru.module').then( m => m.DetailTugasGuruPageModule)
  },
  {
    path: 'tambah-mapel-guru',
    loadChildren: () => import('./tambah-mapel-guru/tambah-mapel-guru.module').then( m => m.TambahMapelGuruPageModule)
  },
  {
    path: 'buat-tugas',
    loadChildren: () => import('./buat-tugas/buat-tugas.module').then( m => m.BuatTugasPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GuruPageRoutingModule {}
