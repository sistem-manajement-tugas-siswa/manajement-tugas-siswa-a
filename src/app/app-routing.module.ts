import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'detail-mapel',
    loadChildren: () => import('./detail-mapel/detail-mapel.module').then( m => m.DetailMapelPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'guru',
    loadChildren: () => import('./guru/guru.module').then( m => m.GuruPageModule)
  },
  {
    path: 'tambah-mapel',
    loadChildren: () => import('./tambah-mapel/tambah-mapel.module').then( m => m.TambahMapelPageModule)
  },
  {
    path: 'kepsek',
    loadChildren: () => import('./kepsek/kepsek.module').then( m => m.KepsekPageModule)
  },
  {
    path: 'gr-wali',
    loadChildren: () => import('./gr-wali/gr-wali.module').then( m => m.GrWaliPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
