import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GrWaliPage } from './gr-wali.page';

const routes: Routes = [
  {
    path: '',
    component: GrWaliPage
  },
  {
    path: 'daftar-mapel',
    loadChildren: () => import('./daftar-mapel/daftar-mapel.module').then( m => m.DaftarMapelPageModule)
  },
  {
    path: 'tab1',
    loadChildren: () => import('./tab1/tab1.module').then( m => m.Tab1PageModule)
  },
  {
    path: 'tab2',
    loadChildren: () => import('./tab2/tab2.module').then( m => m.Tab2PageModule)
  },
  {
    path:'',
    redirectTo: 'gr-wali/tab1',
    pathMatch: 'full'
  },  

  {
    path: 'detail-nilai',
    loadChildren: () => import('./detail-nilai/detail-nilai.module').then( m => m.DetailNilaiPageModule)
  }
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GrWaliPageRoutingModule {}
