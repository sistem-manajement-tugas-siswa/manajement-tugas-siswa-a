import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DaftarMapelPage } from './daftar-mapel.page';

describe('DaftarMapelPage', () => {
  let component: DaftarMapelPage;
  let fixture: ComponentFixture<DaftarMapelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaftarMapelPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DaftarMapelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
