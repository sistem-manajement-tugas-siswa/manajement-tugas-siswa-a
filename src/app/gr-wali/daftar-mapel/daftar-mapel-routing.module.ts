import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DaftarMapelPage } from './daftar-mapel.page';

const routes: Routes = [
  {
    path: '',
    component: DaftarMapelPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DaftarMapelPageRoutingModule {}
