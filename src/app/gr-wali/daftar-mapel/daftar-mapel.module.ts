import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DaftarMapelPageRoutingModule } from './daftar-mapel-routing.module';

import { DaftarMapelPage } from './daftar-mapel.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DaftarMapelPageRoutingModule
  ],
  declarations: [DaftarMapelPage]
})
export class DaftarMapelPageModule {}
