import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GrWaliPage } from './gr-wali.page';

describe('GrWaliPage', () => {
  let component: GrWaliPage;
  let fixture: ComponentFixture<GrWaliPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrWaliPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GrWaliPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
