import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailNilaiPage } from './detail-nilai.page';

const routes: Routes = [
  {
    path: '',
    component: DetailNilaiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailNilaiPageRoutingModule {}
