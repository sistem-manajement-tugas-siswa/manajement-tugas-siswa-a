import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailNilaiPageRoutingModule } from './detail-nilai-routing.module';

import { DetailNilaiPage } from './detail-nilai.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailNilaiPageRoutingModule
  ],
  declarations: [DetailNilaiPage]
})
export class DetailNilaiPageModule {}
