import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GrWaliPageRoutingModule } from './gr-wali-routing.module';

import { GrWaliPage } from './gr-wali.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GrWaliPageRoutingModule
  ],
  declarations: [GrWaliPage]
})
export class GrWaliPageModule {}
