import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';



interface User {
  name: string;
  image: Object;
  ni : string;
  member: string;
 
}
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  user: any = {};
  userData: User;
  member: '';

  constructor(
    public auth : AngularFireAuth,
    public toastController: ToastController,
    public db : AngularFirestore,
    public router : Router
  ) { }

  ngOnInit() {
  }

  async pesanKesalahan() {
    const toast = await this.toastController.create({
      message: 'Tidak dapat melakukan registrasi',
      duration: 2000,
      position: 'middle'
    });
    toast.present();
  }


  loading: boolean;
  registrasi()
  {
    this.loading = true;
    this.auth.createUserWithEmailAndPassword(this.user.email, this.user.password).then(res=>{
      this.writeUser(res.user.email)
    }, err=>{
      this.pesanKesalahan();
      this.loading = false;
    })
  }
  
  updateUserData(email)
  {
    this.auth.onAuthStateChanged(res=>{
      res.updateProfile({displayName:this.user.name});
      this.writeUser(email);
    });
  }

  writeUser(email)
  {
    this.userData = {
      name: this.user.name,
      image:{ url: '',ref: ''},
      ni : this.user.ni,
      member : this.member
    }
    this.db.collection('users').doc(email).set(this.userData).then(res=>{
      this.loading = false;
      this.router.navigate(['./login']);
    }, err=>{
      console.log(err);
      this.pesanKesalahan();
      this.loading = false;
    })
  }

  }