import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth'
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastController } from '@ionic/angular';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  user : any = {};
  userData : any = {};
  constructor(
    public router : Router,
    public auth: AngularFireAuth,
    public db: AngularFirestore,
    public toast : ToastController,
    public getDatabase : AngularFireDatabase

  ) { }

  ngOnInit() {
    
  }


  async pesanKesalahan() {
    const toast = await this.toast.create({
      message: 'Tidak dapat login',
      duration: 2000,
      position: 'middle'
    });
    toast.present();
  }

  loading: boolean;
  login() 
  {
    this.loading = true;
    this.auth.signInWithEmailAndPassword(this.user.email, this.user.password).then(res=> {
      if (this.userData.member == "siswa") {
        this.router.navigate(['./tabs/tab1'])
      }

      else if (this.userData.member == "guru") {
        this.router.navigate(['./guru/tabs1']);
      }

      else if (this.userData.member == "wali"){
        this.router.navigate(['./gr-wali/tab1']);
      }
      
      else {
        this.router.navigate(['./guru/tabs1']);
      }
    }, err=>{
      this.pesanKesalahan();
      this.loading = false;
    })
    
  }

}
