import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModalController, ToastController } from '@ionic/angular';
import { ToastService } from 'src/app/service/toast.service';



@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  @Input() user: any = {};
  @Input() email: any;

  constructor(
    public modalCtrl: ModalController,
    public db: AngularFirestore,
    public toast: ToastService
    

  ) { }

  ngOnInit() {
  }

  loading:boolean;
  saveData()
  {
    this.loading = true;
    this.db.collection('users').doc(this.email).update(this.user).then(res=>{
      this.loading = false;
      this.dismiss();
    }).catch(err=>{
      this.loading = false;
      this.toast.present('Tidak dapat menyimpan data, coba lagi.','top');
    })
  }

  dismiss()
  {
    this.modalCtrl.dismiss();
  }


}
