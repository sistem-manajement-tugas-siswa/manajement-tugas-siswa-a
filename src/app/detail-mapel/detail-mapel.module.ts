import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailMapelPageRoutingModule } from './detail-mapel-routing.module';

import { DetailMapelPage } from './detail-mapel.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailMapelPageRoutingModule
  ],
  declarations: [DetailMapelPage]
})
export class DetailMapelPageModule {}
