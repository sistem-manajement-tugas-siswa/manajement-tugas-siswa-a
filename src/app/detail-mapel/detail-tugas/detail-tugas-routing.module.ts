import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailTugasPage } from './detail-tugas.page';

const routes: Routes = [
  {
    path: '',
    component: DetailTugasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailTugasPageRoutingModule {}
