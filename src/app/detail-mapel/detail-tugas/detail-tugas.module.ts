import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailTugasPageRoutingModule } from './detail-tugas-routing.module';

import { DetailTugasPage } from './detail-tugas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailTugasPageRoutingModule
  ],
  declarations: [DetailTugasPage]
})
export class DetailTugasPageModule {}
