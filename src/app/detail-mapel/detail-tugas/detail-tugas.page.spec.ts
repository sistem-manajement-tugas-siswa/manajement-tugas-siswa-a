import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailTugasPage } from './detail-tugas.page';

describe('DetailTugasPage', () => {
  let component: DetailTugasPage;
  let fixture: ComponentFixture<DetailTugasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailTugasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailTugasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
