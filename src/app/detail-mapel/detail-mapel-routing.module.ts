import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailMapelPage } from './detail-mapel.page';

const routes: Routes = [
  {
    path: '',
    component: DetailMapelPage
  },
  {
    path: 'detail-tugas',
    loadChildren: () => import('./detail-tugas/detail-tugas.module').then( m => m.DetailTugasPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailMapelPageRoutingModule {}
