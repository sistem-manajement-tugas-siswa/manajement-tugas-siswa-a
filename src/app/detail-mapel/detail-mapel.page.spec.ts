import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailMapelPage } from './detail-mapel.page';

describe('DetailMapelPage', () => {
  let component: DetailMapelPage;
  let fixture: ComponentFixture<DetailMapelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailMapelPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailMapelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
