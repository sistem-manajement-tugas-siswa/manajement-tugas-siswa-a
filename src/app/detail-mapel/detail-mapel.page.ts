import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { Router } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-detail-mapel',
  templateUrl: './detail-mapel.page.html',
  styleUrls: ['./detail-mapel.page.scss'],
})
export class DetailMapelPage implements OnInit {

  constructor(
    public actionSheetController: ActionSheetController,
    public router: Router,
    public auth: AngularFireAuth,
    public db: AngularFirestore,
    public storage: AngularFireStorage

  ) { 
    this.getAuthData();
  }

  ngOnInit() {
  }
  selectedSegment: any = 'forum';

  userdata: any = {};
  getAuthData()
  {
    this.auth.onAuthStateChanged(res=>{
      this.userdata = res;
      this.getUserData(res.email);      
      this.getData();
      this.getDataTugas();
    })
  };

  getUserData(email)
  {
    this.db.collection('users').doc(email).get().subscribe(res=>{
      this.passRole(res.data());
    })
  }
  passRole(data)
  {
    this.userdata.role = data.role;
  }

  mapels: any = [];
  getData()
  {
    this.db.collection('mapels').valueChanges({idField: 'id'}).subscribe(res=>{
      this.mapels = res;
    });
  }

  tasks : any = [];
  getDataTugas() {
    this.db.collection('tasks').valueChanges({idField: 'id'}).subscribe(res=>{
      this.tasks = res;
    });
  }


}
