import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TambahMapelPage } from './tambah-mapel.page';

describe('TambahMapelPage', () => {
  let component: TambahMapelPage;
  let fixture: ComponentFixture<TambahMapelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TambahMapelPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TambahMapelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
