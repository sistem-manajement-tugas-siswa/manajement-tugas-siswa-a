import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TambahMapelPage } from './tambah-mapel.page';

const routes: Routes = [
  {
    path: '',
    component: TambahMapelPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TambahMapelPageRoutingModule {}
