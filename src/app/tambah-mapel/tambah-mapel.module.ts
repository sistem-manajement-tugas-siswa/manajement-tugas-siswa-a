import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TambahMapelPageRoutingModule } from './tambah-mapel-routing.module';

import { TambahMapelPage } from './tambah-mapel.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TambahMapelPageRoutingModule
  ],
  declarations: [TambahMapelPage]
})
export class TambahMapelPageModule {}
