import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KepsekPageRoutingModule } from './kepsek-routing.module';

import { KepsekPage } from './kepsek.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KepsekPageRoutingModule
  ],
  declarations: [KepsekPage]
})
export class KepsekPageModule {}
