import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DataNilaiPageRoutingModule } from './data-nilai-routing.module';

import { DataNilaiPage } from './data-nilai.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DataNilaiPageRoutingModule
  ],
  declarations: [DataNilaiPage]
})
export class DataNilaiPageModule {}
