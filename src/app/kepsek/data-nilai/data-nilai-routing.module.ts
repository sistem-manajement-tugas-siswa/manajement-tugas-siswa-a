import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DataNilaiPage } from './data-nilai.page';

const routes: Routes = [
  {
    path: '',
    component: DataNilaiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DataNilaiPageRoutingModule {}
