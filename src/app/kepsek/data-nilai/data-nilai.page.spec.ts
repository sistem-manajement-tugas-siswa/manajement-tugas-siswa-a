import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DataNilaiPage } from './data-nilai.page';

describe('DataNilaiPage', () => {
  let component: DataNilaiPage;
  let fixture: ComponentFixture<DataNilaiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataNilaiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DataNilaiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
