import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { KepsekPage } from './kepsek.page';

describe('KepsekPage', () => {
  let component: KepsekPage;
  let fixture: ComponentFixture<KepsekPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KepsekPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(KepsekPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
