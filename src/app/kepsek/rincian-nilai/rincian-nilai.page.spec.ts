import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RincianNilaiPage } from './rincian-nilai.page';

describe('RincianNilaiPage', () => {
  let component: RincianNilaiPage;
  let fixture: ComponentFixture<RincianNilaiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RincianNilaiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RincianNilaiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
