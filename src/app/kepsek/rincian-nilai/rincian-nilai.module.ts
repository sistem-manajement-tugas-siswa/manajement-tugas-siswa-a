import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RincianNilaiPageRoutingModule } from './rincian-nilai-routing.module';

import { RincianNilaiPage } from './rincian-nilai.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RincianNilaiPageRoutingModule
  ],
  declarations: [RincianNilaiPage]
})
export class RincianNilaiPageModule {}
