import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RincianNilaiPage } from './rincian-nilai.page';

const routes: Routes = [
  {
    path: '',
    component: RincianNilaiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RincianNilaiPageRoutingModule {}
