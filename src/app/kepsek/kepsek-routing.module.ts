import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KepsekPage } from './kepsek.page';

const routes: Routes = [
  {
    path: '',
    component: KepsekPage
  },
  {
    path: 'tab1',
    loadChildren: () => import('../Kepsek/tab1/tab1.module').then( m => m.Tab1PageModule)
  },
  {
    path: 'tab2',
    loadChildren: () => import('../Kepsek/tab2/tab2.module').then( m => m.Tab2PageModule)
  },
  {
    path: 'data-nilai',
    loadChildren: () => import('../Kepsek/data-nilai/data-nilai.module').then( m => m.DataNilaiPageModule)
  },
  {
    path: '',
    redirectTo: '/kepsek/tab1',
    pathMatch: 'full'
  },
  {
    path: 'rincian-nilai',
    loadChildren: () => import('../Kepsek/rincian-nilai/rincian-nilai.module').then( m => m.RincianNilaiPageModule)
  }

    
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KepsekPageRoutingModule {}
